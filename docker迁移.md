# docker迁移
## 基本信息
docker的默认安装路径为 /var/lib/docker
默认子目录如下：
```
drwx------  3 root root 4096 Sep  5 12:35 containers
drwx------  3 root root 4096 Aug 28 10:19 image
drwxr-x---  3 root root 4096 Aug 28 10:19 network
drwx------ 12 root root 4096 Sep  5 12:35 overlay
drwx------  4 root root 4096 Aug 28 10:19 plugins
drwx------  2 root root 4096 Aug 28 10:19 swarm
drwx------  2 root root 4096 Sep  5 11:48 tmp
drwx------  2 root root 4096 Aug 28 10:19 trust
drwx------  2 root root 4096 Aug 28 10:19 volumes
```

## 迁移方案1 - mount新目录
```
# 停止docker服务
systemctl stop dcoker

# 迁移docker目录到数据盘vdb2
mv /var/lib/docker/* /mnt/vdb2/docker/

# 挂载docker目录至原目录
mount --bind /mnt/vdb2/docker /var/lib/docker

# 添加挂载命令至开机脚本（需要此文件有执行权限）
vi /etc/rc.local

# 在文件最后一行添加以下挂载命令
mount --bind /mnt/vdb2/docker /var/lib/docker

# 保存退出

# 添加执行权限
chmod +x /etc/rc.d/rc.local
```

## 迁移方案2 - 修改配置 docker.service
```
# 停止docker服务
systemctl stop dcoker

# 迁移docker目录到数据盘vdb2
mv /var/lib/docker/* /mnt/vdb2/docker/

# 进入配置文件目录
cd /etc/systemd/system/multi-user.target.wants

# 修改配置文件
vim docker.service

# --graph=/data/docker： docker新的存储位置
# --storage-driver=overlay ： 当前docker所使用的存储驱动
ExecStart=/usr/bin/dockerd --graph=/data/docker --storage-driver=overlay

# 重新加载配置
systemctl daemon-reload

# 重启docker
systemctl restart docker

# 检查结果
docker info
```

## 迁移方案3 - 修改配置 /etc/docker/daemon.json
```
vim /etc/docker/daemon.json

# 重启docker
systemctl restart docker

# 检查结果
docker info
```